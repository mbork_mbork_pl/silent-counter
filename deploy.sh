#!/bin/bash
set -e
BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. "$BASE"/.env.development.local
ssh "$TARGET" 'rm -rf ~/public'
scp -r "$BASE"/build "$TARGET":~/public
