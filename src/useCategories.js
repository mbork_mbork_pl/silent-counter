import { useState } from 'react';
import localforage from 'localforage';
import {getExampleCategories} from './exampleCategories';

function useCategories() {
	const [categories, setCategories] = useState(null);
	const saveCategories = function(new_categories) {
		if (!new_categories) {
			new_categories = getExampleCategories();
		}
		localforage
			.setItem('categories', new_categories)
			.then(() => setCategories(new_categories));
	}
	if (!categories) {
		localforage
			.getItem('categories')
			.then(savedCategories => {
				if (savedCategories) {
					setCategories(savedCategories);
				} else {
					const initialCategories = getExampleCategories();
					saveCategories(initialCategories);
				}
			});
	}
	return [categories, saveCategories];
}

export default useCategories;
