import React from 'react';
import {useNavigate} from 'react-router-dom';
import localforage from 'localforage';
import cn from 'classnames';
import useCategories from './useCategories';

function ClearAllData({isClearAllDataModalActive, toggleClearAllDataModal}) {
	const [categories, setCategories] = useCategories();
	const navigate = useNavigate();

	const clearAllData = async () => {
		for (const category of categories) {
			await localforage.removeItem(`categoryData.${category.id}`);
		}
		await localforage.removeItem('categories');
		setCategories(null);
		toggleClearAllDataModal();
		navigate('/');
	};

	return (
		<div className={cn('modal', {'is-active': isClearAllDataModalActive})}>
			<div className="modal-background"></div>
			<div className="modal-card">
				<header className="modal-card-head">
					<p className="modal-card-title">Clear all data</p>
					<button className="delete" aria-label="close" onClick={toggleClearAllDataModal}></button>
				</header>
				<section className="modal-card-body">
					<p>Are you sure you want to clear all data?</p>
					<p>This cannot be undone!</p>
				</section>
				<footer className="modal-card-foot">
					<button className="button is-success" onClick={clearAllData}>Clear data</button>
					<button className="button" onClick={toggleClearAllDataModal}>Cancel</button>
				</footer>
			</div>
		</div>
	);
}

export default ClearAllData;
