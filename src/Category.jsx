import React, {useState} from 'react';
import {useParams} from "react-router-dom";
import dayjs from 'dayjs';
import useCategory from './useCategory';
import Version from './Version';
// import './Category.css';

function Category() {
	const now = dayjs();
	const RANGES = [
		{name: 'last hour', threshold: now.subtract(1, 'hour').unix()},
		{name: 'last 2 hours', threshold: now.subtract(2, 'hour').unix()},
		{name: 'last 3 hours', threshold: now.subtract(3, 'hour').unix()},
		{name: 'today', threshold: now.startOf('day').unix()},
		{name: 'yesterday', threshold: now.startOf('day').subtract(1, 'day').unix()},
		{name: 'this week', threshold: now.startOf('week').unix()},
		{name: 'this month', threshold: now.startOf('month').unix()},
		{name: 'this year', threshold: now.startOf('year').unix()},
		{name: 'all time', threshold: 0},
	];
	RANGES.reverse();
	const DEFAULT_RANGE = 'today';
	const {categoryId} = useParams();
	const [category, setCategory] = useCategory(categoryId);
	const [range, setRange] = useState(DEFAULT_RANGE);

	const threshold = rangeName => RANGES.find(range => range.name === rangeName).threshold;

	if (!category) {
		return null;
	}

	// Convert the entries to an object
	const entries = category.entries.reduce((acc, val) => {
		acc[val.id] = {name: val.name, count: 0};
		return acc;
	}, {});

	// Count the entries' datapoints
	category.datapoints
		.filter(({timestamp}) => timestamp >= threshold(range))
		.forEach(({id}) => {
			entries[id].count++;
		});

	let newEntry, handleChange, addNewEntry; // TODO: implement

	const incrementCounter = entryId => {
		const datapoints = [...category.datapoints];
		datapoints.push({id: entryId, timestamp: now.unix()});
		setCategory({...category, datapoints});
	};

	const handleRangeChange = (e) => {
		const newIndex = e.target.value;
		setRange(RANGES[newIndex].name);
	};

	return (
		<div className="Category">
			<div className="columns is-mobile">
				<div className="title column has-text-centered">{category.name}</div>
			</div>
			<div className="columns is-mobile">
				<div className="column is-two-thirds">
					<input value={RANGES.findIndex(r => r.name === range)} type="range" className="input" min="0" max={RANGES.length - 1} onChange={handleRangeChange}/>
				</div>
				<div className="column is-one-third">
					<span>{range}</span>
				</div>
			</div>
			{Object.keys(entries).map(key =>
				<div className="columns is-mobile" key={key}>
					<div className="column is-four-fifths">
						<button className="button is-light is-fullwidth">
							<span>{entries[key].name}:&nbsp;</span>
							<span className="has-text-weight-bold">{entries[key].count}</span>
						</button>
					</div>
					<div className="column">
						<button className="button is-primary is-fullwidth add-button" onClick={() => incrementCounter(key)}>
							<span className="is-size-5">+</span>
						</button>
					</div>
				</div>
			)}
			<div className="columns is-mobile mt-4">
				<div className="column is-four-fifths">
					<input className="input" value={newEntry} onChange={handleChange} placeholder="New entry"/>
				</div>
				<div className="column">
					<button className="button is-primary is-fullwidth add-button" disabled={!newEntry} onClick={addNewEntry}>
						<span className="is-size-5">+</span>
					</button>
				</div>
			</div>
			<Version/>
		</div>
	);
}

export default Category;
