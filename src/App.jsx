import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import localforage from 'localforage';
import useCategories from './useCategories';
import Version from './Version';
import './App.css';

function App() {
	const [categories, setCategories] = useCategories();

	const [newCategory, setNewCategory] = useState('');
	const handleChange = (e) => setNewCategory(e.target.value);

	const addNewCategory = async() => {
		const maxId = Math.max(...categories.map(({id}) => id));
		const newId = maxId + 1;
		const newCategories = [...categories, {id: newId, name: newCategory}];
		setCategories(newCategories);
		setNewCategory('');
		await localforage.setItem(`categoryData.${newId}`, []);
	};

	const deleteCategory = (idToDelete) => async() => {
		const newCategories = categories.filter(({id}) => id !== idToDelete);
		setCategories(newCategories);
		await localforage.setItem('categories', newCategories);
		await localforage.removeItem(`categoryData.${idToDelete}`);
	};

	if (!categories) {
		return null;
	}

	return (
		<div className="App">
			<div className="columns is-mobile">
				<div className="title column has-text-centered">Categories</div>
			</div>
			{categories.map(({id, name}) =>
				<div className="columns is-mobile" key={id}>
					<div className="column is-four-fifths">
						<Link className="button is-light is-fullwidth" to={`/${id}`}>{name}</Link>
					</div>
					<div className="column">
						<button className="button is-primary is-fullwidth" disabled={!!newCategory} onClick={deleteCategory(id)}>
							<span className="is-size-5">x</span>
						</button>
					</div>
				</div>)}
			<div className="columns is-mobile mt-4">
				<div className="column is-four-fifths">
					<input className="input" value={newCategory} onChange={handleChange} placeholder="New category"/>
				</div>
				<div className="column">
					<button className="button is-primary is-fullwidth add-button" disabled={!newCategory} onClick={addNewCategory}>
						<span className="is-size-5">+</span>
					</button>
				</div>
			</div>
			<Version/>
		</div>
	);
}

export default App;
