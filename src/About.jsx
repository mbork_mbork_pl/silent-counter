import React from 'react';
import cn from 'classnames';

function About({isAboutModalActive, toggleAboutModal}) {
	return (
		<div className={cn('modal', {'is-active': isAboutModalActive})}>
			<div className="modal-background"></div>
			<div className="modal-card">
				<header className="modal-card-head">
					<p className="modal-card-title">About</p>
					<button className="delete" aria-label="close" onClick={toggleAboutModal}></button>
				</header>
				<section className="modal-card-body">
					<p>
						<i>Silent Counter</i> is an app for counting things.  You can use it to count
						how many trains you have spotted, how many times you called your friends, how
						many cups of tea you have drunk, or how many aliens you have encountered.
					</p>
					<p>
						This is a <i>frontend app</i>.  This means that after you load it
						from at {process.env.REACT_APP_URL}, it no longer contacts the server – all data
						is kept on your device and nothing is shared or sent anywhere.
						(This also means that if you use another device, all your data is gone.)
					</p>
					<p>
						<i>Silent Counter</i> is created
						by <a href="http://mbork.pl" rel="noreferrer" target="_blank">
							   Marcin "mbork" Borkowski
						   </a>.
					</p>
				</section>
			</div>
		</div>
	);
}

export default About;
