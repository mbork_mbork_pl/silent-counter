import React, {useState} from 'react';
import {Outlet, Link} from 'react-router-dom';
import cn from 'classnames';
import About from './About.jsx';
import ClearAllData from './ClearAllData.jsx';
import './Navbar.css';

function Navbar() {
	const [isMenuActive, setMenuActive] = useState(false);
	const toggleMenu = () => {
		setMenuActive(state => !state);
	};

	const [isAboutModalActive, setAboutModalActive] = useState(false);
	const toggleAboutModal = () => {
		setAboutModalActive(state => !state);
	};

	const [isClearAllDataModalActive, setClearAllDataModalActive] = useState(false);
	const toggleClearAllDataModal = () => {
		setClearAllDataModalActive(state => !state)
	};

	return <>
		<nav className="navbar" role="navigation" aria-label="main navigation">
			<div className="navbar-brand">
				<Link className="navbar-item" to={'/'}>
					Silent Counter
				</Link>

				{/* eslint-disable-next-line */}
				<a
					role="button"
					onClick={toggleMenu}
					className={cn('navbar-burger', {'is-active': isMenuActive})}
					aria-label="menu" aria-expanded="false" data-target="navbarBasicExample"
				>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</a>
			</div>

			<div id="navbarBasicExample" className={cn('navbar-menu', {'is-active': isMenuActive})}>
				<div className="navbar-start">
					{/* eslint-disable-next-line */}
					<a role="button" className="navbar-item" onClick={() => {toggleAboutModal(); toggleMenu()}}>
						About
					</a>

					{/* eslint-disable-next-line */}
					<a role="button" className="navbar-item" onClick={() => {toggleClearAllDataModal(); toggleMenu()}}>
						Clear all data
					</a>
				</div>
			</div>
		</nav>
		<About isAboutModalActive={isAboutModalActive} toggleAboutModal={toggleAboutModal} />
		<ClearAllData isClearAllDataModalActive={isClearAllDataModalActive} toggleClearAllDataModal={toggleClearAllDataModal} />
		<Outlet/>
	</>;
}

export default Navbar;
