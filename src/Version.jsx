import './Version.css';

function Version() {
	return <div className="version">version: {process.env.REACT_APP_VERSION}</div>
}

export default Version;
