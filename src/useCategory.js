import { useState } from 'react';
import localforage from 'localforage';
import {getExampleCategoryData} from './exampleCategories';
import useCategories from './useCategories';

function useCategory(categoryId) {
	const categoryKey = `categoryData.${categoryId}`;
	const [category, setCategory] = useState(null);
	const [categories] = useCategories();
	let categoryName = '';
	if (categories) {
		categoryName = categories.find(category => category.id === +categoryId).name;
	}
	const saveCategory = function(category) {
		localforage
			.setItem(categoryKey, category)
			.then(() => setCategory(category));
	}
	if (!category) {
		localforage
			.getItem(categoryKey)
			.then(savedCategory => {
				if (savedCategory) {
					setCategory(savedCategory);
				} else {
					const initialCategoryData = getExampleCategoryData(categoryId);
					saveCategory(initialCategoryData);
				}
			});
	} else if (!category.name) {
		setCategory({...category, name: categoryName});
	}
	return [category, saveCategory];
}

export default useCategory;
