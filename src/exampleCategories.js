const exampleCategories = [
	{
		id: 0,
		name: 'Spotted trains',
		entries: [
			{id: 0, name: 'Passenger'},
			{id: 1, name: 'Cargo'},
			{id: 2, name: 'Other'},
		],
		datapoints: [
			{id: 1, timestamp: 1654248300},
			{id: 0, timestamp: 1654498800},
			{id: 0, timestamp: 1654499400},
			{id: 0, timestamp: 1654499700},
		],
	},
	{
		id: 1,
		name: 'Friends to call',
		entries: [
			{id: 0, name: 'Rose'},
			{id: 1, name: 'Martha'},
			{id: 2, name: 'Donna'},
			{id: 3, name: 'Amy'},
			{id: 4, name: 'Clara'},
		],
		datapoints: [],
	},
];

export function getExampleCategories() {
	return exampleCategories.map(({id, name}) => ({id, name}));
};

export function getExampleCategoryData(categoryId) {
	const categoryData = exampleCategories.find(category => category.id === +categoryId);
	const {entries, datapoints} = categoryData;
	return {entries, datapoints};
}
